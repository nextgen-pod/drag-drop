import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SortablejsModule } from 'ngx-sortablejs';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DragDropService } from './shared/drag-drop.service';
import { SidePalleteComponent } from './side-pallete/side-pallete.component';
import { WorkspaceComponent } from './workspace/workspace.component';

@NgModule({
  declarations: [AppComponent, SidePalleteComponent, WorkspaceComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DragDropModule,
    FlexLayoutModule,
    SortablejsModule,
  ],
  providers: [DragDropService],
  bootstrap: [AppComponent],
})
export class AppModule {}

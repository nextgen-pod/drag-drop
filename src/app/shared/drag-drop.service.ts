import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DragDropService {
  constructor() {}

  // drop(event: CdkDragDrop<string[]>) {
  //   console.log('Event', event);
  //   if (event.previousContainer === event.container) {
  //     moveItemInArray(
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   } else {
  //     transferArrayItem(
  //       event.previousContainer.data,
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   }
  // }

  moveInList(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }
  addToList(event: CdkDragDrop<string[]>) {
    this.cloneToList(
      event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }

  cloneToList<T = any>(
    currentArray: T[],
    targetArray: T[],
    currentIndex: number,
    targetIndex: number
  ): void {
    const to = this.clamp(targetIndex, targetArray.length);

    if (currentArray.length) {
      targetArray.splice(to, 0, currentArray[currentIndex]);
    }
  }

  /** Clamps a number between zero and a maximum. */
  clamp(value: number, max: number): number {
    return Math.max(0, Math.min(max, value));
  }
}

import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { DragDropService } from '../shared/drag-drop.service';

@Component({
  selector: 'app-side-pallete',
  templateUrl: './side-pallete.component.html',
  styleUrls: ['./side-pallete.component.scss'],
})
export class SidePalleteComponent implements OnInit {
  list = [
    {
      title: 'BCP',
      url: '/assets/images/bcp.svg',
      description: 'tesing description',
    },
    {
      title: 'GMA',
      url: './assets/images/GMA.svg',
      description: 'tesing description',
    },
    {
      title: 'PMR',
      url: './assets/images/pmr.svg',
      description: 'tesing description',
    },
    {
      title: 'WatchList',
      url: '/assets/images/watchlistReport.svg',
      description: 'tesing description',
    },

    {
      title: 'PACE',
      url: '/assets/images/pace.svg',
      description: 'tesing description',
    },
    {
      title: 'Resume',
      url: '/assets/images/Resume.svg',
      description: 'tesing description',
    },
    {
      title: 'SRS',
      url: './assets/images/srs.svg',
      description: 'tesing description',
    },
  ];
  constructor(private dragDrop: DragDropService) {}

  ngOnInit(): void {}

  // drop(event: CdkDragDrop<string[]>) {
  //   this.dragDrop.drop(event);
  // }
  drop(event: CdkDragDrop<string[]>) {
    this.dragDrop.moveInList(event);
  }
}

import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { DragDropService } from '../shared/drag-drop.service';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.scss'],
})
export class WorkspaceComponent implements OnInit {
  done = [
    {
      title: 'Recog',
      url: './assets/images/recog.png',
      description: 'tesing description',
    },
    {
      title: 'Staffing',
      url: './assets/images/staffing.svg',
      description: 'tesing description',
    },
  ];

  constructor(private dragDrop: DragDropService) {}

  ngOnInit(): void {}

  // drop(event: CdkDragDrop<string[]>) {
  //   this.sharedService.drop(event);
  // }

  drop(event: CdkDragDrop<string[]>) {
    console.log(event);

    // if drop event is from an item that was already on canvas

    console.log(this.done[event.currentIndex].title);

    if (event.container === event.previousContainer) {
      // sort it based on where it was dropped
      this.dragDrop.moveInList(event);
    } else {
      // else the event was from a dropped palette item so add it to the list
      this.dragDrop.addToList(event);
    }
  }
}
